package myhometest3.tests;

import myhometest3.base.TestBaseSetup;
import myhometest3.base.TestData;
import myhometest3.pageobjects.DBSourcePage;
import myhometest3.pageobjects.LogInPage;
import myhometest3.pageobjects.MainPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GotoDBSourcePageTest1 extends TestBaseSetup {
    private WebDriver driver;
    private LogInPage logInPage;
    private MainPage mainPage;
    private DBSourcePage dbSourcePage;
    private String userName;
    private String password;

    @BeforeClass
    public void setUp(){
        driver = getDriver();
        userName = TestData.userName;
        password = TestData.password;
    }

    @Test
    public void verifyGotoPage(){
        logInPage = new LogInPage(driver);
        logInPage = new LogInPage(driver);
        mainPage = logInPage.autoLogIn(userName,password);
        mainPage.clickMenuIcon();
        mainPage.clickSystemSettingButton();
        dbSourcePage = mainPage.clickDBSourceButton();
        Assert.assertTrue(dbSourcePage.verifyDBSourceMainElementPresence(),"Go to the DB Source page failed");


    }
}
