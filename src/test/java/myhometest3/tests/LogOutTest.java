package myhometest3.tests;

import myhometest3.base.TestBaseSetup;
import myhometest3.base.TestData;
import myhometest3.pageobjects.LogInPage;
import myhometest3.pageobjects.MainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LogOutTest  extends TestBaseSetup {

    private WebDriver driver;
    private LogInPage logInPage;
    private MainPage mainPage;
    private String userName;
    private String password;

    @BeforeClass
    public void setUp(){
        driver = getDriver();
        userName = TestData.userName;
        password = TestData.password;
    }

    @Test
    public void verifyLogOutFunction() {
        logInPage = new LogInPage(driver);
        mainPage = logInPage.autoLogIn(userName,password);
        mainPage.clickUserIcon();
        mainPage.clickLogOutButton();
        Assert.assertTrue(mainPage.verifyLogOut(), "Log out action failed");

    }

}
