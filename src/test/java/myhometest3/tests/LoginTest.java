package myhometest3.tests;

import myhometest3.base.TestBaseSetup;
import myhometest3.base.TestData;
import myhometest3.pageobjects.LogInPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class LoginTest extends TestBaseSetup {

    private WebDriver driver;
    private String userName;
    private String password;

    @BeforeClass
    public void setUp(){
    driver = getDriver();
    userName = TestData.userName;
    password = TestData.password;
}

    @Test
    public void verifyLoginFunction() {
        System.out.println("LOG IN page test...");
        LogInPage logInPage = new LogInPage(driver);
        logInPage.enterUserName(userName);
        logInPage.enterPassword(password);
        logInPage.clickLogInButton();
        Assert.assertTrue(logInPage.verifyGoToMainPage(), "Log in action failed");

    }


}
