package myhometest3.tests;

import myhometest3.base.TestBaseSetup;
import myhometest3.base.TestData;
import myhometest3.pageobjects.GlobalPanel;
import myhometest3.pageobjects.LogInPage;
import myhometest3.pageobjects.MainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SearchProjectTest extends TestBaseSetup {
    private WebDriver driver;
    private LogInPage logInPage;
    private MainPage mainPage;
    private String userName;
    private String password;
    private String project;

    @BeforeClass
    public void setUp() {
        driver = getDriver();
        userName = TestData.userName;
        password = TestData.password;
        project = TestData.projectName;
    }

    @Test
    public void verifySearchProject() {
        System.out.println("SEARCH PROJECT test...");
        logInPage = new LogInPage(driver);
        mainPage = logInPage.autoLogIn(userName,password);
        mainPage.clickMenuIcon();
        mainPage.clickSearchIcon();
        mainPage.enterSeekingProject(project);
        Assert.assertTrue(mainPage.verifySearchResult(project),"Project is not found");
    }
}