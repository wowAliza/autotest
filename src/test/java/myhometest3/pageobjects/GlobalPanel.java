package myhometest3.pageobjects;

import myhometest3.base.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GlobalPanel {

    protected WebDriver driver;
    private By menuIcon = By.xpath("//*[@id='ic-hamburger-menu']");
    private By searchIcon = By.xpath("//*[@id='cv_drawer_menu_state_button_Search']");
    private By textBoxSearch = By.xpath(" //*[contains(text(),'Type here and press enter')]/following::input");
    private By userIcon = By.xpath(" //*[@id='header-user-icon']//*[@class='v-icon']");
    private By logOutButton = By.xpath(" //*[@id=\"ic-logout-last_Log_out\"]");
    private By systemSettingButton = By.xpath("//*[@id='ic-global-setting_System_Settings']");
    private By dbSourseButton = By.xpath("//*[@id='DB_Source']");

    public GlobalPanel(WebDriver driver){
        this.driver = driver;
    }

    public void clickMenuIcon(){
        WebElement userNameBox = driver.findElement(menuIcon);
        if (userNameBox.isDisplayed()){
            userNameBox.click();
        } else {
            System.out.println("Menu button not found");
        }
    }

    public void clickSearchIcon(){
        WebElement checkboxSearch = driver.findElement(searchIcon);
        if (checkboxSearch.isDisplayed()){
            checkboxSearch.click();
        } else {
            System.out.println("Search icon not found");
        }
    }

    public void enterSeekingProject(String seekingProject){
        WebElement textAriaElement = driver.findElement(textBoxSearch);
        if (textAriaElement.isDisplayed()){
            textAriaElement.sendKeys(seekingProject, Keys.ENTER);;
        } else {
            System.out.println("Text aria for Search not found");
        }
    }

    public boolean verifySearchResult(String expectedProject){
        String projectFound = driver.findElement(By.xpath("//span[@class='cv-drawer-branch-result-caption-project-title'][1]")).getText();
        return projectFound.equals(expectedProject);
    }

    public void clickSystemSettingButton(){
        WebElement systemSettingElement = driver.findElement(systemSettingButton);
        if (systemSettingElement.isDisplayed()){
            systemSettingElement.click();
        } else {
            System.out.println("System Setting button not found");
        }
    }

     public DBSourcePage clickDBSourceButton(){
        WebElement dbSourceButtonElement = driver.findElement(dbSourseButton);
        if (dbSourceButtonElement.isDisplayed() || dbSourceButtonElement.isEnabled()){
            dbSourceButtonElement.click();
        } else {
            System.out.println("DB Source button not found");
        }
        return new DBSourcePage(driver);
    }

    public void clickUserIcon(){
        WebElement userIconElement = driver.findElement(userIcon);
        if (userIconElement.isDisplayed()){
            userIconElement.click();
        } else {
            System.out.println("User icon not found");
        }
    }

    public void clickLogOutButton(){
        WebElement logOutElement = driver.findElement(logOutButton);
        if (logOutElement.isDisplayed()){
            logOutElement.click();
        } else {
            System.out.println("LogOut button not found");
        }
     }

     public boolean verifyLogOut(){
        WebElement logoElement = driver.findElement(LogInPage.logoItem);
        return logoElement.isDisplayed();
     }



}
