package myhometest3.pageobjects;

import myhometest3.base.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {

    protected WebDriver driver;
    public static By logoItem = By.xpath(" //*[@class='login-logo']");
    private By userNameField = By.id("username");
    private By passwordField = By.id("password");
    private By logInButton = By.xpath("//*[@class = 'login-submit']");
    private By expectedPageItem = By.xpath("//*[contains(text(),'Welcome to')]");

    public LogInPage(WebDriver driver){
        this.driver = driver;
    }


    public boolean verifyGoToMainPage() {
        WebElement expectedElement = driver.findElement(expectedPageItem);
        return (expectedElement.isDisplayed());
    }

    public void enterUserName(String userName){
        WebElement userNameBox = driver.findElement(userNameField);
        if (userNameBox.isDisplayed()){
            userNameBox.sendKeys(userName);
        } else {
            System.out.println("User Name input field not found");
        }
    }

    public void enterPassword(String password){
        WebElement passwordBox = driver.findElement(passwordField);
        if (passwordBox.isDisplayed()){
            passwordBox.sendKeys(password);
        } else {
            System.out.println("Password input field not found");
        }
    }

    public void clickLogInButton(){
        WebElement loginBtn = driver.findElement(logInButton);
        if (loginBtn.isDisplayed() || loginBtn.isEnabled()){
            loginBtn.click();
        } else {
            System.out.println("LOG IN button not found");
        }
    }

    public MainPage autoLogIn(String userName, String password){
        enterUserName(userName);
        enterPassword(password);
        clickLogInButton();
        return new MainPage(driver);
    }


}
