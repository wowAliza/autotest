package myhometest3.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class DBSourcePage extends GlobalPanel{

    protected WebDriver driver;
    private By pageItem = By.xpath("//span[@class='v-text v-text-left' and text()='DB Sources']");

    public DBSourcePage(WebDriver driver){
        super(driver);
    }

    @Override
    public DBSourcePage clickDBSourceButton() {
        return super.clickDBSourceButton();
    }

    public WebElement getElement(){
        return driver.findElement(pageItem);
    }

    public boolean verifyDBSourceMainElementPresence() {
        WebElement mainPageElement = driver.findElement(pageItem);
        return (mainPageElement.isDisplayed() || mainPageElement.isEnabled());
    }



}