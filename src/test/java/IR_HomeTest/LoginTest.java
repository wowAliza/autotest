package IR_HomeTest;

import myhometest3.base.TestBaseSetup;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginTest  {
    WebDriver driver;

        public WebDriver setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\yanin\\\\Drivers\\" + "chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().to("https://hrk-cv-dev6f.axiomsl.us:8081/cv/ui/global/index");
        return driver;
    }


    // тест, который проверяет 1 позитивный кейс поиска проекта
    @Test
    public void checkProjectFound() {
        String projectName = "CBC_020";
        driver = setUp();

        try {
            driver.findElement(By.id("username")).sendKeys("oooo");
            driver.findElement(By.id("password")).sendKeys("oooo");
            driver.findElement(By.xpath(" //*[@id=\"lower\"]/input")).click();


            driver.findElement(By.xpath(" //*[@id=\"gwt-uid-3\"]")).click();
            driver.findElement(By.xpath(" //*[contains(text(),'Search')]/../*[1]")).click();
            driver.findElement(By.xpath(" //*[contains(text(),'Type here and press enter')]/following::input")).sendKeys(projectName, Keys.ENTER);


            String text = driver.findElement(By.xpath(" //*[contains(@class,'cv-drawer-branch-result-caption-project-title')]")).getText();
            Assert.assertEquals(text, projectName);

        } finally {
            driver.quit();
        }
    }


    // тест, который проверяет, что страница DBSource открылась
    @Test
    public void checkDBSourcePage(){
        driver = setUp();

        try {
            driver.findElement(By.id("username")).sendKeys("oooo");
            driver.findElement(By.id("password")).sendKeys("oooo");
            driver.findElement(By.xpath(" //*[@id=\"lower\"]/input")).click();

            driver.findElement(By.xpath(" //*[@id=\"gwt-uid-3\"]")).click();
            driver.findElement(By.xpath("//*[@id='ic-global-setting_System_Settings']")).click();
            driver.findElement(By.xpath("//*[@id='DB_Source']")).click();

            WebElement element = driver.findElement(By.xpath("//span[@class='v-text v-text-left' and text()='DB Sources'])"));
            Assert.assertTrue(element.isDisplayed(),"Error");

        } finally {
            driver.quit();

        }
    }


    //тест, который проверяет, что залогиненный юзер может успешно вылогиниться
    @Test
    public void checkLogOut(){
        driver = setUp();

        try {
            driver.findElement(By.id("username")).sendKeys("oooo");
            driver.findElement(By.id("password")).sendKeys("oooo");
            driver.findElement(By.xpath(" //*[@id=\"lower\"]/input")).click();

            driver.findElement(By.xpath(" //*[@id='header-user-icon']//*[@class='v-icon']")).click();
            driver.findElement(By.xpath(" //*[@id=\"ic-logout-last_Log_out\"]")).click();

            Boolean hasLogo = driver.findElement(By.xpath(" //*[@class='login-logo']")).isDisplayed();
            Assert.assertTrue(hasLogo,"Test successful");

        } finally {
            driver.quit();
        }
    }


}