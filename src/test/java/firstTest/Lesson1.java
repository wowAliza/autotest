package firstTest;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Lesson1 {

        @DataProvider
        public Object[][] getData(){
            return new Object[][]{
                    {"username1","password1"},
                    {"username2","password2"},
                    {"username3","password4"},
            };
        }


        @Test (dataProvider = "getData")
        public void checkRegister(String name, String password){
            System.out.println(name + " " + password);
        }

        @Test
        public void firstTest() {
            WebDriver driver = new ChromeDriver();
            try {
                driver.get("https://www.google.com/");
                WebElement element = driver.findElement(By.id("logo-default"));
                boolean isdisplayed = element.isDisplayed();
                Assert.assertTrue(isdisplayed);
            } finally {
                driver.quit();
            }
        }

        @Test
        public void secondTest () {
            WebDriver driver = new ChromeDriver();
            try {
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                driver.get("https://www.ukr.net/");
                String s = "https://www.ukr.net/news/main.html";
//                driver.switchTo().frame("mail widget");
//                WebElement element = driver.findElement(By.id("id-input-password"));
//                element.sendKeys("test"); // ввод пароля
                driver.findElement(By.xpath(" //*[contains(text(),'Головне')]")).click();
                driver.switchTo().window(driver.getWindowHandle());
                String ss = driver.getCurrentUrl();
                Assert.assertEquals(ss,s);

            } finally {
                driver.quit();
            }
        }

        @Test
        public void thirdTest(){
            WebDriver driver = new ChromeDriver();
            try {

                driver.get("https://www.ukr.net/");
                new WebDriverWait(driver,20)
                        .until(ExpectedConditions.visibilityOf(
                                driver.findElement(By.xpath("xxx"))
                        ));
            } finally {
                driver.quit();
            }
        }

        @Test
        public void fourTest(){
            WebDriver driver = new ChromeDriver();

            driver.get("https://www.ukr.net/");
            new WebDriverWait(driver,20)
                    .ignoring(ElementNotVisibleException.class)
                    .until(new ExpectedCondition<Boolean>() {

                               public Boolean apply(WebDriver driver1){
                                   WebElement element = driver1.findElement(By.id("XX"));
                                   element.getAttribute("color")
                                           .equalsIgnoreCase("red");
                                   return element.isDisplayed();
                               }
                           }
                    );
        }}



